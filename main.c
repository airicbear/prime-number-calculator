#include <stdio.h>

int is_prime(int num) {
    if (num < 2) {
        return 0;
    }
    for (int i = 2; i < num; i++) {
        if (num % i == 0) {
            return 0;
        }
    }
    return 1;
}

void print_primes(int num_primes) {
    int count = 0;
    int i = 2;
    while (count < num_primes) {
        if (is_prime(i)) {
            printf("%d ", i);
            count++;
        }
        i++;
    }
}

int main(void) {
    int num_primes = 0;
    printf("Number of prime numbers: ");
    scanf("%d", &num_primes);
    print_primes(num_primes);
    return 0;
}
